import "./App.css";
import { useState } from "react";
import { useFormik, Formik, Field, Form, FieldArray } from "formik";

function App() {
  let originalItems = [
    {
      label: "Bracelets",
      id: 145689,
      checked: false,
      children: [
        {
          label: "Jasinthe Bracelet",
          id: 145690,
          checked: false,
        },
        {
          label: "Jasinthe Bracelet",
          id: 145691,
          checked: false,
        },
        {
          label: "Inspire Bracelet",
          id: 145692,
          checked: false,
        },
      ],
    },
    {
      label: "",
      id: 145693,
      checked: false,
      children: [
        {
          label: "Recurring Item",
          id: 145694,
          checked: false,
        },
        {
          label: "Recurring Item with questions",
          id: 145695,
          checked: false,
        },
        {
          label: "Zero amount item with questions",
          id: 145696,
          checked: false,
        },
        {
          label: "Normal item with questions",
          id: 145697,
          checked: false,
        },
        {
          label: "normal item",
          id: 145698,
          checked: false,
        },
      ],
    },
  ];
  const [items, setItems] = useState(originalItems);

  const handleSearch = (input) => {};
  return (
    // <div className="App">
    //   <header className="App-header">
    //     <img src={logo} className="App-logo" alt="logo" />
    //     <p>
    //       Edit <code>src/App.js</code> and save to reload.
    //     </p>
    //     <a
    //       className="App-link"
    //       href="https://reactjs.org"
    //       target="_blank"
    //       rel="noopener noreferrer"
    //     >
    //       Learn React
    //     </a>
    //   </header>
    // </div>
    <div style={{ width: "100%" }}>
      <div className="container">
        <p className="container__header"> Add Tax</p>
        <Formik
          initialValues={{
            taxName: "",
            taxPercent: 5,
            picked: "ALL",
            applied_to: "some",
            checked: [],
            items: items,
          }}
          onSubmit={(values) => {
            alert(
              `{ \n applicable_item: ${JSON.stringify(
                values.checked
              )} \n applied_to: ${values.applied_to} \n name: ${
                values.taxName
              } \n rate: ${JSON.stringify(values.taxPercent / 100)} \n }`
            );
            // handle form submission
          }}
        >
          {({ values }) => (
            <Form>
              <div className="container__first__section">
                {/* container which holds the input fields */}
                <div className="first__section--flex">
                  {/* first field */}
                  <div className="first__section--flex__block">
                    <Field
                      type="text"
                      placeholder="Tax Name"
                      className={"container__input__field1"}
                      name="taxName"
                    />
                  </div>
                  {/* second input field */}
                  <div className="first__section--flex__block2">
                    <Field
                      type="number"
                      className="container__input__field2"
                      name="taxPercent"
                    />
                    <span className="container__input__field2__icon">%</span>
                  </div>
                </div>

                {/* radio button group */}
                <div
                  class="container__radio__button"
                  role="group"
                  aria-labelledby="my-radio-group"
                  onChange={(e) => {
                    // check if applied_to is all or some
                    if (e.target.value == "all") {
                      // if all check the parents and push the checked one to the array
                      values.items.map((item, index) => {
                        values.items[index].checked = true;
                        values.checked.push([...values.items[index].children]);
                      });

                      setItems([...values.items]);
                    } else {
                      // uncheck the parents
                      values.items.map((item, index) => {
                        values.items[index].checked = false;
                      });

                      setItems([...values.items]);
                    }
                  }}
                >
                  <label class="radio__button_label--one">
                    <div class="radio__button">
                      <Field
                        name="applied_to"
                        type="radio"
                        class="radio__button--field"
                        value="all"
                      />
                      <div
                        color="#F16D37"
                        class={
                          values.applied_to == "all"
                            ? "svg__checked"
                            : "svg__hidden"
                        }
                      >
                        <svg
                          color="#F16D37"
                          viewBox="0 0 24 24"
                          class="radio__button__svg"
                        >
                          <polyline points="20 6 9 17 4 12"></polyline>
                        </svg>
                      </div>
                    </div>
                    <span style={{ marginLeft: "0.5rem" }}>
                      Apply to all items in collection
                    </span>
                  </label>
                  <label style={{ display: "block" }}>
                    <div class="radio__button">
                      <Field
                        name="applied_to"
                        type="radio"
                        class="radio__button--field"
                        value="some"
                      />
                      <div
                        color="#F16D37"
                        class={
                          values.applied_to == "some"
                            ? "svg__checked"
                            : "svg__hidden"
                        }
                      >
                        <svg
                          color="#F16D37"
                          viewBox="0 0 24 24"
                          class="radio__button__svg"
                        >
                          <polyline points="20 6 9 17 4 12"></polyline>
                        </svg>
                      </div>
                    </div>
                    <span style={{ marginLeft: "0.5rem" }}>
                      Apply to specific items
                    </span>
                  </label>
                </div>
              </div>
              <hr />
              {/* section 2 */}
              <div className="container__second__section">
                {/* search bar section */}
                <div class="container__search">
                  <div class="container__search__bar">
                    <span class="search__bar__icon">
                      <svg
                        stroke="currentColor"
                        fill="currentColor"
                        stroke-width="0"
                        viewBox="0 0 1024 1024"
                        class="text-md text-gray-500"
                        height="1em"
                        width="1em"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path d="M909.6 854.5L649.9 594.8C690.2 542.7 712 479 712 412c0-80.2-31.3-155.4-87.9-212.1-56.6-56.7-132-87.9-212.1-87.9s-155.5 31.3-212.1 87.9C143.2 256.5 112 331.8 112 412c0 80.1 31.3 155.5 87.9 212.1C256.5 680.8 331.8 712 412 712c67 0 130.6-21.8 182.7-62l259.7 259.6a8.2 8.2 0 0 0 11.6 0l43.6-43.5a8.2 8.2 0 0 0 0-11.6zM570.4 570.4C528 612.7 471.8 636 412 636s-116-23.3-158.4-65.6C211.3 528 188 471.8 188 412s23.3-116.1 65.6-158.4C296 211.3 352.2 188 412 188s116.1 23.2 158.4 65.6S636 352.2 636 412s-23.3 116.1-65.6 158.4z"></path>
                      </svg>
                    </span>

                    <input
                      className="container__section__search"
                      placeholder="Search Items"
                      onChange={(e) => {
                        // get the input and initialize new items
                        let input = e.target.value;
                        let newItems = [];

                        // map over the items
                        values.items.map((item, index) => {
                          // check if the parent checkboxes contain the search key
                          if (
                            item.label
                              .toLowerCase()
                              .includes(input.toLowerCase())
                          ) {
                            newItems = [...newItems, item];
                          } else {
                            // check if children checkboxes contain the seaarch key
                            let children = [];
                            item.children.map((child, i) => {
                              if (
                                child.label
                                  .toLowerCase()
                                  .includes(input.toLowerCase())
                              ) {
                                children.push(child);
                                // newItems.push(item);
                              }
                            });

                            // check if the children array length is greater than 0
                            if (children.length > 0) {
                              values.items[index].children = children;
                              newItems.push(values.items[index]);
                            }
                          }
                        });

                        if (input.length > 0) {
                          values.items = newItems;
                          setItems([...newItems]);
                        } else {
                          values.items = originalItems;
                          setItems([...originalItems]);
                        }
                      }}
                    />
                  </div>
                </div>
                <FieldArray
                  name="items"
                  render={(arrayHelpers) => (
                    <div className="container__checkbox__section">
                      {values.items.map((item, index) => {
                        return (
                          <div className="">
                            <div>
                              <div class="main__checkbox">
                                <label class="checkbox__label">
                                  <div class="checkbox__block">
                                    <input
                                      type="checkbox"
                                      class="checkbox__input"
                                      onChange={(e) => {
                                        // toggle parent check box
                                        values.items[index].checked =
                                          !values.items[index].checked;

                                        //  map over children checkboxes and push them to the checked array
                                        values.items[index].children.map(
                                          (child) => {
                                            // check if the child item is already under checked array
                                            let findIndex =
                                              values.checked.findIndex(
                                                (val) => val == child.id
                                              );

                                            // if the current parent check box is checked and if the child item is not under checked array, push it
                                            if (values.items[index].checked) {
                                              if (findIndex === -1) {
                                                values.checked.push(child.id);
                                              }
                                            } else {
                                              // if the parent is unchecked and the child is under the checked array, remove the child from the checked array
                                              if (findIndex >= 0) {
                                                values.checked.splice(
                                                  findIndex,
                                                  1
                                                );
                                              }
                                            }
                                          }
                                        );

                                        // set items
                                        setItems([...values.items]);
                                      }}
                                    />
                                    <div color="#327B91" class="checkbox__svg ">
                                      <svg
                                        color="#327B91"
                                        viewBox="0 0 24 24"
                                        class={
                                          values.items[index]?.checked
                                            ? " checkbox__svg--checked"
                                            : ""
                                        }
                                      >
                                        <polyline points="20 6 9 17 4 12"></polyline>
                                      </svg>
                                    </div>
                                  </div>
                                  <span
                                    class="ml-4"
                                    style={{ marginLeft: "1rem" }}
                                  >
                                    {item.label}
                                  </span>
                                </label>
                              </div>
                              {/* other */}
                              <div
                                role="group"
                                aria-labelledby="checkbox-group"
                              >
                                {item?.children.map((child, i) => {
                                  return (
                                    <div
                                      class="main__checkbox"
                                      style={{ paddingLeft: "1rem" }}
                                    >
                                      <label
                                        class="checkbox__label"
                                        style={{ background: "none" }}
                                      >
                                        <div class="checkbox__block">
                                          <Field
                                            type="checkbox"
                                            class="checkbox__input"
                                            onChange={(e) => {
                                              // check and uncheck child checkboxes
                                              values.items[index].children[
                                                i
                                              ].checked =
                                                !values.items[index].children[i]
                                                  .checked;

                                              //  check if the current child is under the checked arrauy
                                              let findIndex =
                                                values.checked.findIndex(
                                                  (val) => val == child.id
                                                );

                                              // check if the current item is checked
                                              if (
                                                values.items[index].children[i]
                                                  .checked
                                              ) {
                                                // if the child is not under the checked array
                                                if (findIndex == -1) {
                                                  values.checked.push(child.id);
                                                }
                                              } else {
                                                if (findIndex != -1) {
                                                  values.checked.splice(
                                                    findIndex,
                                                    1
                                                  );
                                                }
                                              }

                                              // set items
                                              setItems([...values.items]);
                                            }}
                                          />
                                          <div
                                            color="#327B91"
                                            class="checkbox__svg"
                                          >
                                            <svg
                                              color="#327B91"
                                              viewBox="0 0 24 24"
                                              class={
                                                values.items[index]?.checked ||
                                                values.items[index].children[i]
                                                  ?.checked
                                                  ? " checkbox__svg--checked"
                                                  : ""
                                              }
                                            >
                                              <polyline points="20 6 9 17 4 12"></polyline>
                                            </svg>
                                          </div>
                                        </div>
                                        <span
                                          class="ml-4"
                                          style={{ marginLeft: "1rem" }}
                                        >
                                          {child.label}
                                        </span>
                                      </label>
                                    </div>
                                  );
                                })}
                              </div>
                            </div>
                          </div>
                        );
                      })}
                    </div>
                  )}
                />
                {/* */}
                {/* checkbox section */}
              </div>
              <div class="container__button">
                <button type="submit" color="#F16D37" class="submit__button">
                  Apply tax to {values.checked?.length} item(s)
                </button>
              </div>
            </Form>
          )}
        </Formik>

        <div className="container__section__3"></div>
      </div>
    </div>
  );
}

export default App;
